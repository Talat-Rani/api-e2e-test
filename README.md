# Description
Cypress Framework
Electron browser
This project contains automated tests [GitHub Gists API](https://developer.github.com/v3/gists/) 
using tool [cypress.io](https://www.cypress.io).
(https://docs.cypress.io/api/commands/click#Syntax). 

## Tools
- Language: JavaScript
- Test framework: [cypress.io](https://www.cypress.io/)
- IDE 

## Installation and execution

### Install prerequisites

 1. Install git
 [https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
  2. Install docker
 [https://docs.docker.com/install/](https://docs.docker.com/install/)


### Run tests

1. Clone this git repository:
 ```bash
 mkdir ~/gitrepo
 cd ~/gitrepo
 git clone git@bitbucket.org:Talat-Rani/api-e2e-test.git
 ```

2. Run in docker:

```bash
cd ~/gitrepo/api-e2e-test/
npx cypress run --record --key 4834bcea-3f13-4d33-8893-7a7be3c992e7

npx cypress run --spec /Users/talatr/Sites/bitbucket/apiIntergrationTest/api-e2e-test/cypress/integration/api-automation_test.js --record --key 4834bcea-3f13-4d33-8893-7a7be3c992e7

```

### Reporting 

- ScreenShots
- Video recordings 
- live test coverage on https://dashboard.cypress.io/projects/957wqk



### Test framework selection reasoning

Cypress Pros:
1. Easy to start (very good documentation)
2. Familiar with this tool
3. Open source, big community
4. Quick test execution
5. Covers required functionality
6. Has many advanced features to cover more complicated tests in the future


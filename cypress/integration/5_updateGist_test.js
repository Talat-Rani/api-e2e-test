/// <reference types="Cypress" />

import { postGist, doDeleteGist, checkRateLimit, patchGist, assertGist } from '../support/gistFunctions';
import { authHeader } from '../support/common';

describe('PATCH https://api.github.com/gists/:gist_id', function () {

    let gistId

    let originalGistBody = {
        "files": {
            "myFile.txtt": {
                "content": "Testing git creation API"
            }
        }
    }

    beforeEach(() => {
        checkRateLimit(authHeader)

        cy.request(
            postGist(originalGistBody, authHeader)
        ).then((resp) => {
            expect(resp.status).to.eq(201)
            gistId = resp.body.id
        })
    })

    it('Test - Update a gist - For authorized call', () => {

        let updatedGistBody = {
            "files": {
                "myFile.txt": {
                    "content": "This file will be updated"
                }
            }
        }

        cy.request(
            patchGist(gistId, authHeader, updatedGistBody)
        ).then((resp) => {
            expect(resp.status).to.eq(200)
            assertGist(resp.body, updatedGistBody)
        })
    })

    afterEach(() => {
        doDeleteGist(gistId, authHeader)
    })
})

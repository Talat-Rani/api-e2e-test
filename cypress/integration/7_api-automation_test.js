import { email, password } from '../support/common';
import {locators} from '../support/locators'

describe('Before Hook', () => {
    before(function () {
        cy.visit('https://github.com/login')
        cy.get(locators.TXT_LOGIN_EMAIL).type(email)
        cy.get(locators.TXT_LOGIN_PASS).type(password)
        cy.get(locators.BTN_SUBMIT_LOGIN).click()
    })
    it('Create a public gist', () => {
        cy.visit('https://gist.github.com/')
        //Url assertion
        cy.url().should('eq', 'https://gist.github.com/')
        cy.get(locators.TXT_GIST_DESC).type('Testing Gist Creation API').should('have.value', 'Testing Gist Creation API')
        cy.get(locators.TXT_GIST_FILENAME).type('myFile.txt').should('have.value', 'myFile.txt')
        cy.get(locators.TXT_GIST_CONTENT).type('This is a test gist')
        cy.get(locators.DROP_DOWN_MENU).click()
        cy.get(locators.BTN_CREATE_PUBLIC_GIST).click()
        cy.get(locators.CREATE_GIST).click()
        //Assert gist created
        cy.get(locators.LBL_MY_GIST).contains('This is a test gist')

        //Get all Gist for your user
        cy.get(locators.BTN_PROFILE).click()
        cy.get(locators.BTN_VIEW_MY_GISTS).click()
        cy.visit('https://gist.github.com/')
        cy.get(locators.BTN_LIST_MY_GISTS).should('be.exist')
        cy.get('body').then(($body) => {
            cy.get(locators.LIST_OF_GISTS).should('be.exist')
        })

        // Update an existing gist
        cy.get(locators.SELECT_GIST).click()
        cy.get(locators.BTN_EDIT_GIST).click()
        // //Url assertion
        cy.url().should('include', '/edit')

        it('Click on View All Gists', () => {
            cy.xpath('//button[@type=\'button\'])[4]').click
        })
        cy.get(locators.TXT_GIST_DESC).clear().type('Testing Gist Update API').should('have.value', 'Testing Gist Update API')
        cy.wait(500)

        it('Add file name .type({selectall}{backspace})', () => {
            cy.get(locators.TXT_GIST_FILENAME).type('clear this')
            cy.get(locators.TXT_GIST_FILENAME).type('{selectall}{backspace}').type('myFile1.txt')
            cy.get(locators.TXT_GIST_FILENAME).should('have.value', 'myFile1.txt')
        })

        it('Add content .type({selectall}{backspace})', () => {
            cy.get(locators.TXT_GIST_CONTENT).type('clear this')
            cy.get(locators.TXT_GIST_CONTENT).type('{selectall}{backspace}').type('This file will be updated')
            cy.get(locators.TXT_GIST_CONTENT).should('have.value', 'This file will be updated')
        })

        //Add second file
        cy.get(locators.BTN_ADD_FILE).click()
        cy.wait(500)
        it('Add second file under the same gist .type({selectall}{backspace})', () => {
            cy.get(locators.ADD_FILE_NAME).type('clear this')
            cy.get(locators.ADD_FILE_NAME).type('{selectall}{backspace}').type('myFile2.txt')
            cy.get(locators.ADD_FILE_NAME).should('have.value', 'myFile2.txt')
        })
        //Type gist code and assert it

        it('add content for the second file .type({selectall}{backspace})', () => {
            cy.get(locators.FILE_CONTENT).type('clear this')
            cy.get(locators.FILE_CONTENT).type('{selectall}{backspace}').type('This file will be deleted')
            cy.get(locators.FILE_CONTENT).should('have.value', 'This file will be deleted')
        })

        cy.get(locators.BTN_SUBMIT_EDIT_GIST).click()

        //Delete Existing Gist
        cy.get(locators.BTN_DELETE_GIST).click()
        //Assert gist deleted
        cy.get('body').contains('Gist deleted successfully.')
    })
})

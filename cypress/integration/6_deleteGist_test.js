/// <reference types="Cypress" />

import { getGist, postGist, deleteGist, checkRateLimit } from '../support/gistFunctions';
import { authHeader } from '../support/common';

describe('DEL https://api.github.com/gists/:gist_id', function () {

    let gistId

    let gistBody = {
        "files": {
            "myFile.txt": {
                "content": "Testing git creation API"
            }
        }
    }

    beforeEach(() => {
        checkRateLimit(authHeader)

        cy.request(
            postGist(gistBody, authHeader)
        ).then((resp) => {
            expect(resp.status).to.eq(201)
            gistId = resp.body.id
        })
    })

    it('Test - Delete a gist - For authorized call', () => {

        cy.request(
            deleteGist(gistId, authHeader)
        ).then((resp) => { expect(resp.status).to.eq(204) })

        cy.request(
            getGist(gistId, authHeader)
        ).then((resp) => { expect(resp.status).to.eq(404) })

    })
})

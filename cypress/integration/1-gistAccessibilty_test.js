/// <reference types="Cypress" />

import { postGist, doDeleteGist, checkRateLimit, getGistsByUserName } from '../support/gistFunctions';
import { authHeader, noAuth, userName } from '../support/common';

describe('GET https://api.github.com/users/:username/gists', function () {

    let gistId

    beforeEach(() => {
        checkRateLimit(authHeader)
        checkRateLimit(noAuth)
    })

    it('Testing git creation API', () => {

        let gistBody = {
            "files": {
                "myFile.txt": {
                    "content": "This is a test Gist"
                }
            }
        }

        cy.request(
            postGist(gistBody, authHeader)
        ).then((resp) => {
            expect(resp.status).to.eq(201)
            gistId = resp.body.id

            cy.request(
                getGistsByUserName(authHeader, userName)
            ).then((resp) => {
                expect(resp.status).to.eq(200)
                expect(gistId).to.be.equal(resp.body[0].id)
                expect(resp.body[0].public).to.be.equal(false)
            })

            cy.request(
                getGistsByUserName(noAuth, userName)
            ).then((resp) => {
                expect(resp.status).to.eq(200)
                expect(gistId).to.be.not.equal(resp.body[0].id)
                expect(resp.body[0].public).to.be.equal(true)
            })

        })
    })

    afterEach(() => {
        doDeleteGist(gistId, authHeader)
    })
})

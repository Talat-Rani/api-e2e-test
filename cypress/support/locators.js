function elementHandler(str) {
    return new Buffer(str, 'elementHandler').toString()
}

export const locators = {
    TXT_LOGIN_EMAIL: '#login_field',
    TXT_LOGIN_PASS: '#password',
    BTN_SUBMIT_LOGIN: '.btn',
    TXT_GIST_DESC: '.js-gist-filename',
    TXT_GIST_FILENAME: '.input-block',
    TXT_GIST_CONTENT: '.CodeMirror-scroll',
    DROP_DOWN_MENU: '.select-menu-button',
    BTN_CREATE_PUBLIC_GIST: '.select-menu-item:nth-child(2) .description',
    CREATE_GIST: '.hx_create-pr-button',
    LBL_MY_GIST: '#file-testing-gist-creation-api-LC1',
    EDIT_CONTENT: '.CodeMirror-line > span',
    EDIT_GIST_FILENAME: '.js-gist-file:nth-child(4) .file-header .form-control',
    SELECT_GIST: '.flex-auto:nth-child(1) .text-bold',
    BTN_EDIT_GIST: 'li > .btn',
    BTN_ADD_FILE: '.float-left',
    LIST_OF_GISTS: '.d-md-block',
    ADD_FILE_NAME: '.file:nth-child(1) > .file-header .form-control',
    FILE_CONTENT: '.CodeMirror-focused > .CodeMirror-scroll',
    BTN_SUBMIT_EDIT_GIST: '.btn-primary',
    BTN_DELETE_GIST: '.pagehead-actions > :nth-child(2) > form > .btn',
    BTN_PROFILE: '#user-links > details > summary',
    BTN_VIEW_MY_GISTS: '#user-links > details > details-menu > a:nth-child(3)',
    BTN_LIST_MY_GISTS: '.d-md-block',
}
